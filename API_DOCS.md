#API Docs

## API BASE URL: https://cupidknot.kuldip.dev/

## 1. Register User
### POST : https://cupidknot.kuldip.dev/api/register 

Parameters
```
rules = {
        'first_name' => 'required|string',
        'last_name' => 'required|string',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|string|min:8|confirmed',
        'password_confirmation' => 'required|string|max:255',
        'birth_date' => 'date:Y-m-d',
        'gender' => 'required|string|in:MALE,FEMALE',
}
```
---
## 2. Login User
### POST : https://cupidknot.kuldip.dev/api/login 

Parameters
```
rules = {
        'username' => 'required|string|max:255',
        'password' => 'required|string|max:255',
}
```
Response: Success
```
{
    "token_type": "Bearer",
    "expires_in": 3600,
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN"
}
```

---
## 3. Refresh API Auth Token 
Access Token will expire in 24 Hrs And Refresh token will expire in 15 days

For refreshing the access token use this route
### POST : https://cupidknot.kuldip.dev/api/refresh 
Parameters
```
rules = {
        'refresh_token' => 'required',
}
```
Response: Success
```
{
    "token_type": "Bearer",
    "expires_in": 3600,
    "access_token": "ACCESS_TOKEN",
    "refresh_token": "REFRESH_TOKEN"
}
```
Error Response: Refresh Token Expired
```
{
    "error": "invalid_request",
    "error_description": "The refresh token is invalid.",
    "hint": "Token has been revoked",
    "message": "The refresh token is invalid."
}
```
---
# Note: Following routes needed authentication so add header parameters 
## 4. Logout
### POST : https://cupidknot.kuldip.dev/api/logout
HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN"
}
```

---
## 5. Own Profile
### GET : https://cupidknot.kuldip.dev/api/user
HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN"
}
```
---
## 6. List of Users
### GET : https://cupidknot.kuldip.dev/api/users
HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN",
}
```
## 7. Update User
### POST : https://cupidknot.kuldip.dev/api/update_user
Ex Url. https://cupidknot.kuldip.dev/api/update_user

HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN",
}
```

Parameters with rules
```
rules = {
    'first_name' => 'string|max:255',
    'last_name' => 'string|max:255'
    'email' => 'string|max:255',
    'religion' => 'string',
    'birth_date' => 'date|Y-m-d',
    'gender' => 'string|in:MALE,FEMALE',
    'updated_at' => 'date|Y-m-d H:i:s',
}
```
## 8. Upload User Images
### POST : https://cupidknot.kuldip.dev/api/user_images
Ex Url. https://cupidknot.kuldip.dev/api/user_images

HEADERS
```
{
    "Accept": "application/json",
    "Authorization": "Bearer ACCESS_TOKEN",
}
```

Parameters with rules
```
rules = {
    'original_photo[]' => 'images' // array of images
}
```

