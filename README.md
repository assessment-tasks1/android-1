# Android App Assessment

#### *Note: Use MVP or MVVM or DDD Design pattern to develop this app

API URL: https://cupidknot.kuldip.dev/api/

### API Documentation

[Documentation](/API_DOCS.md)

This Documentation contains API routes, Method(POST,GET,etc.) & Responses


### Using the Above API documentation Build An Android App with following features.

- Splash Screen
- Register
- Login
- Refresh Login Token
- View Own Profile 
- Update Profile details
- Update Images
- List Users With Images
- Show User Details

## IMP NOTES: 
- After completing the task create public repository in GitHub or Gitlab or Bitbucket and push the code.
- don't send zip file or attachment to email.
- If you don't know about git then you are not eligible. 
- Code should be well formatted and follows Dart standard [guidelines](https://dart.dev/guides/language/effective-dart/style) 
- Handle all Errors and Exceptions with your own knowledge
- If you need sample UI designs please check this [repo](https://gitlab.com/assessment-tasks1/android-ui). Use this as a reference or search in figma community you will find UI designs there.
- Add screenshots or video of your app in repository
- Create a release build & add in the release section
